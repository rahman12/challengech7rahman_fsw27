const { room, user } = require("./pageControllers");
const {User} = require ('../models')
const format = (user)=>{
    const { id, username} = user;

    return {
        id, username, token: user.generateToken(),
    };
};

exports.login = (req, res) => {
    User.authentication (req.body)
    .then((data) =>{
        res.json({ status: "Lowgeen Success", data: format(data)});
    })
    .catch((err)=>{
        res.status(500).json({status: "Lowgeen failed", message: err });

    })
}

exports.auth = (req, res) => {
    console.log("pertama ");
    console.log("kedua");
    setTimeout(() => { console.log("JavaScript")},100)
    console.log("ketiga");
    console.log("keempat");

};

exports.interval = (req, res) =>{
    let count = 0;

const buildInterval = setInterval(()=>{
    console.log (`${++count} running`);
    if (count === 10) clearInterval(buildInterval);
}, 1000); 
}

exports.even = (req, res) =>{
    let count = 0;
    const buildInterval = setInterval(()=>{
        if (count % 2 === 0) console.log(`${count} itu genap`);
        else console.log(`${count} itu ganjil`);
        ++count;
        if( count === 20) clearInterval(buildInterval);
    },500);
}

const checkCredential = ({username, password}) => {
    const data = users.find((row) => row.username === username);
    return new Promise((resolve, reject) =>{
        if(data.password === password) return resolve (data);
        reject("wrong credential");
    });
}

exports.register = (req, res) =>{
    User.register(req.body)
    .then((data)=>{
        res.json({status: "Registration Success", data})
    })
    .catch((err)=>{
        res.status(500).json({status: "Register failed", message: err})
    })
}

exports.timeout = (req, res) =>{
    console.log("pertama");
    console.log("kedua");
    setTimeout(()=>{
        console.log("ketiga");
    },2000);
    setTimeout(()=>{
        console.log("keempat");
    },1000);
    setTimeout(()=>{
        console.log("kelima");
    },3000);
}

exports.getuser = (req, res) => {
    user.findAll({ include: ["username", "password","email"]})
    .then((users)=>{
        res.json({ message: "fetch succes", data: users});
    })
    .catch((err)=>{
        res.status(500).json({status: "fetch failed", message: err});
    });
}

exports.whoami = (req, res ) =>{
    const currentUser = req.user;
    res.json(currentUser);
};