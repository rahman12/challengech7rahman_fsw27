const {page, Room} = require('../models');

exports.get =(req, res) =>{
    page.findAll({include:('Room')}).then((result)=>{
        res.json({status:'fetch Success', result});
    });
}
exports.create = (req, res) => {
    page.create(req.body).then((result)=>{
        res.json({status: 'create Success', result});
    });
}