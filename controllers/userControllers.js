const {User, Room} = require('../models');

exports.get =(req, res) =>{
    User.findAll({include:('User')}).then((result)=>{
        res.json({status:'fetch Success', result});
    });
}
exports.create = (req, res) => {
    User.create(req.body).then((result)=>{
        res.json({status: 'Create User Success', result});
    });
}