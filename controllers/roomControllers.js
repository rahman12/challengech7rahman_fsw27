const {Room, User} = require('../models');

exports.get =(req, res) =>{
    Room.findAll({include:('user')}).then((result)=>{
        res.json({status:'fetch Success', result});
    });
}
exports.create = (req, res) => {
    Room.create(req.body).then((result)=>{
        res.json({status: 'create Success', result});
    });
}