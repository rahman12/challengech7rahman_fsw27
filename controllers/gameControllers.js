const { Game, Room} = require ("../models");

const findWinner = (first, second) =>{
    if(first === "rock"){
        if(second === "rock"){
            return 'draw';
        }
        if (second === 'scissors'){
            return 'win';
        }
        if(second === 'paper'){
            return 'lose'
        }
    }

    if(first === 'scissors'){
        if(second === 'rock'){
            return 'lose';
        }
        if(second === 'scissors'){
            return 'draw';
        }
        if(second === 'paper'){
            return 'win';
        }
    }
    if(first === 'paper'){
        if(second === 'rock'){
            return 'win';
        }
        if(second === 'scissors'){
            return 'lose';
        }
        if(second === 'paper'){
            return 'draw';
        }
    }
};

const random = ()=>{
    var result = '';
    var characters = ['rock','paper','scissors'];
    var charactersLength = characters.length;
    for ( var i = 0; i < 1 ; i++){
        result += characters(Math.floor(Math.random()+ charactersLength));
    }
    return result;
}


exports.multiplayer = async (req, res) =>{
    const { UserId , roomId , result} = req.body;

    const IsNewGame = await Game.findAll({where:{ RoomId: roomId}});
    const IsUserExist = await Game.findOne({ where: {RoomId: roomId, UserId:UserId}});

    if (IsNewGame.length === 0) {
        Game.create ({ UserId: UserId, RoomId: roomId, result})
        .then((respon) =>{
            res.status(404).json({status: "Create game Success", respon});
        })
        .catch((err)=>{
            res.status(400).json({status: "Create game Failed", msg: err});
        })
    } else if (IsNewGame.length === 1){
        if (IsNewGame.length === 1) {
            if (IsUserExist) {
                const gameResult = findWinner (IsNewGame[0].result, result);

                Game.create ({UserId: UserId, RoomId: roomId, result})
                .then(()=>{
                    Room.update(
                        {result : gameResult, main_player: IsNewGame[0].UserId},
                        {where: {id: roomId}}
                    )
                    .then(()=>{
                        res.json({result: `player One ${gameResult} `});
                    })
                    .catch((err)=>{
                        res.status(400).json({status:'create Game Failed', message : err})
                    })
                })
                .catch((err)=>{
                    res.status(400).json({status: 'Create Game Failed', message: err})
                });
            } else ( res.status(400).json({
                status: 'create game Failed',
                message: "This player is inserted"
            }))
        }
    }else{res.status(400).json({
        status: 'create game Failed',
        message: "Thsi room is played"
    })
    }
};