const passport = require ("passport");
const localStrategy = require ("passport-local").Strategy;
const { Strategy : JwtStrategy, ExtractJwt } = require('passport-jwt' )
const { User } =  require ("../models");
const user = require("../models/user");
const Room = require('../controllers/roomControllers');
const page = require('../controllers/pageControllers')

const options = {
    
    jwtFromRequest:ExtractJwt.fromHeader('authorization' ),
    secretOrKey:'this is secret number',
}

passport .use(new JwtStrategy (options, async (payload, done) => {
    User.findByPk(payload.id)
    .then(user => done(null, user))
    .catch(err => done(err, false))
    }))

passport.serializeUser((user, done) => done(null, user.id));
passport.deserializeUser(async(id, done) =>
 done(null, await User.findByPk(id))

);


module.exports = passport;