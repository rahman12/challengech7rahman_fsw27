var express = require('express');
var router = express.Router();

const authRouter = require ('./auth');
const usersRouter = require ('./users');
//const pageRouter = require ('./page')
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.use('/auth',authRouter);
router.use('/user',usersRouter);

module.exports = router;
