const router = require("express").Router();
const auth = require ("../controllers/authControllers");

router.get("/timeout",auth.timeout);
router.get("/interval", auth.interval);
router.get("/even", auth.even);
router.post("/login", auth.login);
router.post("/register", auth.register);
router.get("/login", auth.login);
//router.get("/multiplayer,", auth.multiplayer);
module.exports = router;